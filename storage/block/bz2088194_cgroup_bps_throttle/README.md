# storage/block/bz2088194_cgroup_bps_throttle

Storage: inaccurate bps throttle on cgroup

## How to run it
Please refer to the top-level README.md for common dependencies.

### Install dependencies
```bash
root# bash ../../../cki_bin/pkgs_install.sh metadata
```

### Execute the test
```bash
bash ./runtest.sh
```
